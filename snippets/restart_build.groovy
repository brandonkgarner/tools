/**
 * This script attached to a Jenkins job as the first build step, will cancel any other currently running instances of
 * the same job. This is mainly used for things like dev builds that may already be in progress, but don't need to be
 * deployed twice.
 */

/**************************************
 * RUN THIS AS A SYSTEM GROOVY SCRIPT
 **************************************/
import hudson.model.Result
import jenkins.model.CauseOfInterruption

// Iterate through current project runs
build.getProject()._getRuns().iterator().each{ run ->
  def exec = run.getExecutor()
  // If the run is not a current build and it has executor (running) then stop it
  if( run!=build && exec!=null ){
    // Prepare the cause of interruption
    def cause = { "interrupted by build #${build.getId()}" as String } as CauseOfInterruption
    exec.interrupt(Result.ABORTED, cause)
  }
}