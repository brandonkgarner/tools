
    # If more than 100 entities, split up -- Dynamo batch_get_item restriction
    loop_items = []
    if 'RequestItems' in event:  # SKIP AUTH when admin
        tbl = get_db_client()
        rItems = get_params['RequestItems'][event['TableName']]

        if 'Keys' in rItems:
            while len(rItems['Keys']) > 100:
                t_list = rItems['Keys'][:100]
                rItems['Keys'] = rItems['Keys'][101:]

                small_batch_params = {'RequestItems': {event['TableName']: {'Keys': t_list}}}
                small_batch_items = listDynamoEntities(event, tbl, small_batch_params)
                loop_items = loop_items + small_batch_items

    items = listDynamoEntities(event, tbl, get_params)

    # Include previous loop data if it exists
    if loop_items:
        items = loop_items + items
