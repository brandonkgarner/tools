# ---------- get list of issues in DEVELOPMENT COMPLETE (up to 100) ---------- 
curl -X GET \
  "${JIRA_BASE_URL}/search?jql=project=XXX%26status=%22DEVELOPMENT%20COMPLETE%22&maxResults=100" \
  -H "Authorization: Basic ${ENCODED_CREDENTIALS}" \
  -H 'Accept: application/json' \
  -H 'Cache-Control: no-cache' \
  -H 'Content-Type: application/json' > issues.json

# Get list
arr=$(python ~/Jenkins_Python/get_issue_list.py simple dev)


for i in ${arr}; do
 ---------- change each status to READY FOR QA ---------- 
  curl -X POST \
    "${JIRA_BASE_URL}/issue/${i}/transitions" \
    -H 'Accept: application/json' \
    -H "Authorization: Basic ${JIRA_CREDENTIALS}" \
    -H 'Cache-Control: no-cache' \
    -H 'Content-Type: application/json' \
    -d '{"transition": {"id":"000"}}'
done
